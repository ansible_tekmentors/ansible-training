
#ORACLE_HOME=/ns/apps/rbi/product/fmw/Oracle_BI1; export ORACLE_HOME
#LD_LIBRARY_PATH=/opt/app/home/rbiadmin/instantclient_12_1; export LD_LIBRARY_PATH
#PATH=$PATH:/opt/app/home/rbiadmin/instantclient_12_1; export PATH
#TNS_ADMIN=/ns/apps/rbi/product/fmw/Oracle_BI1/network/admin; export TNS_ADMIN


#SOA_UID=XXRBS_RBISOA_APPUSER
#FCM_UID=XXRBS_RBIFCM_APPUSER
#OFS_UID=XXRBS_RBIOFS_APPUSER
#BI_UID=FIRSTRBI_BIPLATFORM

#SOA_PWD=$(grep SOA $HOME/conf/password_file_${TWO_TASK} | awk -F'=' '{print $2};' | uniq)
#FCM_PWD=$(grep EBS $HOME/conf/password_file_${TWO_TASK} | awk -F'=' '{print $2}' | uniq)
#OFS_PWD=$(grep OFSAA $HOME/conf/password_file_${TWO_TASK} | awk -F'=' '{print $2}' | uniq)
#BI_PWD=$(grep OBI $HOME/conf/password_file_${TWO_TASK} | awk -F'=' '{print $2}' | uniq)



# -----------------------------------------------------------------------------------------------------
function FN_RPD_deploy()
# -----------------------------------------------------------------------------------------------------
{
 RPD_BASE=$SCRIPT_DIR/rpd
 RPD_SRV_LOC=$INSTANCE_HOME/bifoundation/OracleBIServerComponent/coreapplication_obis1/repository
 DFLT_RPD=$(grep '^Star ' $INSTANCE_HOME/config/OracleBIServerComponent/coreapplication_obis1/NQSConfig.INI | cut -f 1 -d, | cut -c 8-)
 DFLT_CHK=$(echo $DFLT_RPD | grep SampleAppLite | wc -l)
 
 # Is the variables file present?
 FN_debug Is the variables file present
 VAR_CHK=$HOME/conf/variables_file_${TWO_TASK}.xml
 if [ -f $VAR_CHK ]
 then
  FN_debug " Variables file present..."
 else
  FN_print " ERROR: No Variables file exiting. Please re-run using option 0 initally."
  exit 1
 fi     

 # Has the variables file been changed?
 FN_debug Has the variables file been changed
 VAR_CHG=$(grep 'CHG_[A-Z,_]*' $HOME/conf/variables_file_${TWO_TASK}.xml | wc -l)
 if [ $VAR_CHG -gt 0 ]
 then
  FN_print "Please make sure variables are updated specific to the target environment. Then re-run the deployment."
  FN_print "Update variables in the $HOME/conf/variables_file_${TWO_TASK}.xml file."
  FN_print "Exiting..."
  exit 1     
 fi
   
 # Is the passwords file present?
 FN_debug Is the passwords file present
 PASS_CHK=$HOME/conf/password_file_${TWO_TASK}
 if [ -f $PASS_CHK ]
 then
  FN_debug " Password file present..."
 else
  FN_print " ERROR: No Password file exiting. Please re-run using option 0 initally."
  exit 1
 fi

 # Have all the passwords been changed?
 FN_debug  Have all the passwords been changed
 PASS_CHG=$(grep 'Change Password' $HOME/conf/password_file_${TWO_TASK} | wc -l)
 if [ $PASS_CHG -gt 0 ]
 then
  FN_print "Please make sure passwords are updated specific to the target environment. Then re-run the deployment."
  FN_print "Update passwords in the $HOME/conf/password_file_${TWO_TASK} file."
  FN_print "Exiting..."
  exit 1     
 fi

 # Install BIServerT2PProvisioner.jar
 FN_debug Check and Install BIServerT2PProvisioner.jar
 BIPR=$MW_HOME/Oracle_BI1/bifoundation/server/bin/BIServerT2PProvisioner.jar
 if [ -f $BIPR ]
 then
  FN_debug " BI Provisioning utility present..."
 else
  FN_debug "Copying BI Provisioning utility from the CEMLI pacakge..."
  cp $RPD_BASE/BIServerT2PProvisioner.jar $BIPR && chmod +x $BIPR
  [ $? -ne 0 ] && { FN_print "Unable to chmod $BIPR"; exit 1; }
 fi

 # Updating the passwords for physical connections in the repository
 FN_debug  Updating the passwords for physical connections in the repository
 cd $INSTANCE_HOME/bifoundation/OracleBIApplication/coreapplication/setup
 . ./bi-init.sh

# All edited out as password check functionality is not available yet.

 # Checking for SQL*Plus
# FN_debug Checking the system for SQL*Plus
# SQLPLUS_CHK=$(which sqlplus | grep sqlplus | wc -l)
# if [ $SQLPLUS_CHK -eq 1 ]
# then
# FN_print "\nSQL*PLus exists continuing...."
# else
# FN_print "\nSQL*Plus is missing please ensure it's availability before continuing."
# exit 1
#fi

 # Check entered DB passwords are correct
# FN_debug Checking entered DB passwords are correct

# FN_print "\nPlease wait while I check the validity of your DB passwords.\n"

#FN_debug Checking SOA password
#SQLPLUS_OUTPUT_SOA=$(sqlplus -s "$SOA_UID/$SOA_PWD@SOA" <<ENDOFSQL
#select bingo from dual;
#exit;
#ENDOFSQL
#)

#if [ -n "$(printf '%s\n' $SQLPLUS_OUTPUT_SOA | grep bingo)" ]; then
#    echo -e "SOA Password is \033[32mcorrect.\n" 2>&1 | tee $RPD_BASE/pass_check
#    tput sgr0
#else
#    echo -e "SOA Password is \033[31mwrong.\n" 2>&1 | tee $RPD_BASE/pass_check
#    tput sgr0
#fi

#FN_debug Checking EBS password
#SQLPLUS_OUTPUT_FCM=$(sqlplus -s "$FCM_UID/$FCM_PWD@EBS" <<ENDOFSQL
#select bingo from dual;
#exit;
#ENDOFSQL
#)

#if [ -n "$(printf '%s\n' $SQLPLUS_OUTPUT_FCM | grep bingo)" ]; then
#    echo -e "EBS Password is \033[32mcorrect.\n" 2>&1 | tee --append $RPD_BASE/pass_check
#    tput sgr0
#else
#    echo -e "EBS Password is \033[31mwrong.\n" 2>&1 | tee --append $RPD_BASE/pass_check
#    tput sgr0
#fi

#FN_debug Checking OFSAA password
#SQLPLUS_OUTPUT_OFS=$(sqlplus -s "$OFS_UID/$OFS_PWD@OFSA" <<ENDOFSQL
#select bingo from dual;
#exit;
#ENDOFSQL
#)

#if [ -n "$(printf '%s\n' $SQLPLUS_OUTPUT_OFS | grep bingo)" ]; then
#    echo -e "OFS Password is \033[32mcorrect.\n" 2>&1 | tee --append $RPD_BASE/pass_check
#    tput sgr0
#else
#    echo -e "OFS Password is \033[31mwrong.\n" 2>&1  | tee --append $RPD_BASE/pass_check
#    tput sgr0
#fi

#FN_debug Checking OBI password
#SQLPLUS_OUTPUT_BI=$(sqlplus -s "$BI_UID/$BI_PWD@OBIEE" <<ENDOFSQL
#select bingo from dual;
#exit;
#ENDOFSQL
#)

#if [ -n "$(printf '%s\n' $SQLPLUS_OUTPUT_BI | grep bingo)" ]; then
#    echo -e "BI Password is \033[32mcorrect.\n" 2>&1 | tee --append $RPD_BASE/pass_check 
#    tput sgr0
#else
#    echo -e "BI Password is \033[31mwrong.\n" 2>&1 | tee --append $RPD_BASE/pass_check
#    tput sgr0
#fi

#grep -q 'wrong' $RPD_BASE/pass_check
#if [[ $? -eq 0 ]] ; then
# echo "
#****************************************************************************************************
#
# There is a possible problem with the passwords. 
# Please check $HOME/conf/password_file_$TWO_TASK fix and then re-run. 
#
# Exiting.....
#
#****************************************************************************************************
#"
#exit
#else
#echo -e "All passwords are \033[32mcorrect"
#tput sgr0
#fi

#FN_debug Removing pass_check
#rm -rf $RPD_BASE/pass_check

 echo "
#****************************************************************************************************
#
# Pre-requisite checks have passed OK. Deploying the new repository...
#
#****************************************************************************************************
# Script will update the following repository components:
#
#     1. Passwords for environment specific physical connection pools - (encrypted)
#     2. Environment specific variables (e.g. DSNs, Users)
#     3. Repository password
#
#****************************************************************************************************
#
#****************************************************************************************************
 "

# Commented out as per DBA's request.
# while true
# do
#  read -p "Enter current RPD password : " RPDPWD
RPDPWD=Admin123
  FN_print "\nINFO: 1. Passwords for environment specific physical connection pools...\n"
  cd $RPD_BASE
  FN_debug PWD $(pwd)
  FN_debug java -jar $MW_HOME/Oracle_BI1/bifoundation/server/bin/BIServerT2PProvisioner.jar -passwords $PASS_CHK -input OracleBIAnalyticsApps_BI0003.rpd -output $DFLT_RPD.withpassword 
# The below was modified to suppress the output of BIServerT2PProvisioner.jar by adding >> $LOG_FILE - 16-12-15
  FN_print "\nINFO: 1. Please be patient......."
  echo $RPDPWD | java -jar $MW_HOME/Oracle_BI1/bifoundation/server/bin/BIServerT2PProvisioner.jar -passwords $PASS_CHK -input OracleBIAnalyticsApps_BI0003.rpd -output $DFLT_RPD.withpassword >> $LOG_FILE 
  [ $? -ne 0 ] && \
   { FN_print "\nERROR: There was a problem running BIServerT2PProvisioner.jar - see logfile $LOG_FILE for more details.\n"; } || \

PASSWORD_RPD=$(grep "^Star = " $INSTANCE_HOME/config/OracleBIServerComponent/coreapplication_obis1/NQSConfig.INI|awk '{print $3}'|sed 's/,//'| sed 's/.rpd//')
ADMIN_HOST=$(grep -A12 "<machine>ADMINHOST</machine>" $DOMAIN_HOME/aserver/bifoundation_domain/config/config.xml| grep listen-address | sed 's/<[^>]*>//g' | awk ' { print $1 } ')
ADMIN_PORT=$(grep adminPort $INSTANCE_HOME/config/OPMN/opmn/instance.properties|sed 's/adminPort=//')

# Commented out as now using secure userconfigfile and userkeyfile so no need for username and password.
# Generate Variable for use in python script
#read -p "Enter WebLogic Administrator user : " WLSADMIN
#read -p "Enter password : " WLSPWD

if [ $DFLT_CHK = 1 ]
then
FN_debug "\nINFO: Not generating script as initial deployment."
FN_debug "\nINFO: Setting password variable."
CURR_PASS=Admin123
else
FN_debug "\nINFO: Generating Password Python Script"
# Generate PY Script to get current RPD Password
#  echo "user = '"$WLSADMIN"'" >RPD_Password.py
#  echo "password = '"$WLSPWD"'" >>RPD_Password.py
  echo "host = '"$ADMIN_HOST":"$ADMIN_PORT"'" >RPD_Password.py
  echo "connect(userConfigFile='"$DOMAIN_HOME"/aserver/bifoundation_domain/configfileWL.secure',userKeyFile='"$DOMAIN_HOME"/aserver/bifoundation_domain/keyfile.secure',url=host)" >>RPD_Password.py
  echo "listCred(map='oracle.bi.enterprise',key='repository."$PASSWORD_RPD"')" >>RPD_Password.py
$MW_HOME/oracle_common/common/bin/wlst.sh RPD_Password.py > rpd_password.log
CURR_PASS=$(grep "PASSWORD" rpd_password.log | awk -F ':' '{ print $2 }')
fi
 
 # 1.  Passwords for environment specific physical connection pools - (encrypted)    
 FN_debug "INFO: Passwords for environment specific physical connection pool"
 mkdir -p $RPD_BASE/archive
 [ ! -f $RPD_BASE/archive/$DFLT_RPD.original ] && \
  { FN_debug "Backing-up original release RPD"; cp $RPD_SRV_LOC/$DFLT_RPD $RPD_BASE/archive/$DFLT_RPD.original; }
 [ ! -f  $RPD_BASE/archive/$DFLT_RPD.original ] && { FN_print "ERROR: RPD not backed up to $RPD_BASE/archive/$DFLT_RPD.original"; exit 1; }
 cp $RPD_BASE/$DFLT_RPD.withpassword $RPD_BASE/archive/$DFLT_RPD.withpassword
 mv $RPD_BASE/$DFLT_RPD.withpassword $RPD_BASE/$DFLT_RPD
 [ $? -ne 0 ] && { FN_print "ERROR: mv $RPD_BASE/$DFLT_RPD.withpassword $RPD_BASE/$DFLT_RPD - FAILED!"; exit 1; } || \
  FN_debug "\nINFO: mv $RPD_BASE/$DFLT_RPD.withpassword $RPD_BASE/$DFLT_RPD okay"

 FN_print "\nINFO: 1. Passwords for environment specific physical connection pools... (SUCCESS)\n\n"


# 2. Updating the environment variables in the repository
 FN_print "INFO: 2. Updating environment variables in the repository...\n"
 cd $INSTANCE_HOME/bifoundation/OracleBIApplication/coreapplication/setup
 . ./bi-init.sh
 
 cd $MW_HOME/Oracle_BI1/bifoundation/server/bin/
 ./biserverxmlexec -P $RPDPWD -I $VAR_CHK -B $RPD_BASE/$DFLT_RPD -O $RPD_BASE/$DFLT_RPD.withvars >> $LOG_FILE
 [ $? -ne 0 ] && 
  { FN_print "ERROR: /biserverxmlexec -P $RPDPWD -I $VAR_CHK -B $RPD_BASE/$DFLT_RPD -O $RPD_BASE/$DFLT_RPD.withvar - FAILED!"; exit 1; } || \
  FN_debug "INFO: biserverxmlexec okay"

 cp $RPD_BASE/$DFLT_RPD.withvars $RPD_BASE/archive/$DFLT_RPD.withvars
 rm $RPD_BASE/$DFLT_RPD
 mv $RPD_BASE/$DFLT_RPD.withvars $RPD_BASE/$DFLT_RPD 
 [ $? -ne 0 ] && { FN_print "mv $RPD_BASE/$DFLT_RPD.withvars $RPD_BASE/$DFLT_RPD - FAILED!"; exit 1; } || FN_debug "mv $RPD_BASE/$DFLT_RPD.withvars okay"

 FN_print "INFO: 2. Updating environment variables in the repository... (SUCCESS)\n\n"



 #3. Repository password
 FN_print "\n\nINFO: 3. Repository password...\n"    
 
#while true
# do
	if [ -z "$1" ] 
	then
		read -p "Would you like to change the RPD password to that of the current deployed rpd(y|n) ? " CHANGE_RPD
	else
		CHANGE_RPD=$1
	fi 
 
 case "$CHANGE_RPD" in
  [yY])
 FN_print "\n\nINFO: 3. Repository password...\n"    
 FN_print "
#************************************ Change the RPD Password ****************************************
#
# This allows you to change the RPD password in this CEMLI and deploy the updated RPD file.
# 
# Please follow the instructions on screen, and enter:
# - the current RPD password
# - the new RPD password
#
#****************************************************************************************************"

  echo ; echo 
  echo "INFO: Current RPD password = $RPDPWD"; echo
# tee is supressing output so user does not know what they're doing
#  ./obieerpdpwdchg -I $RPD_BASE/$DFLT_RPD -O $RPD_BASE/$DFLT_RPD.newpass | tee -a $LOG_FILE
printf "Admin123\n"$CURR_PASS"" | ./obieerpdpwdchg -I $RPD_BASE/$DFLT_RPD -O $RPD_BASE/$DFLT_RPD.newpass
  [ $? -eq 0 ] && \
   { FN_print "\nINFO: 3. Repository password... (SUCCESS)\n"; } ||
   { FN_print "ERROR: Problem changing password - see $LOG_FILE"; exit 1; } 
    break;;
  [nN])
    FN_print "\nINFO: Change Password: ${CHANGE_RPD}"
    FN_print "\nINFO: 3. Repository password... (SKIPPED)\n"
    cp $RPD_BASE/$DFLT_RPD $RPD_BASE/$DFLT_RPD.newpass
    break;;
  esac
#done 

 cp $RPD_BASE/$DFLT_RPD.newpass $RPD_BASE/archive/$DFLT_RPD.newpass
 rm $RPD_BASE/$DFLT_RPD
 mv $RPD_BASE/$DFLT_RPD.newpass $RPD_BASE/$DFLT_RPD 
 [ $? -ne 0 ] && { FN_print "mv $RPD_BASE/$DFLT_RPD.newpass $RPD_BASE/$DFLT_RPD - FAILED!"; exit 1; } || FN_debug "mv $RPD_BASE/$DFLT_RPD.newpass okay"
 
 # Deploy RPD to Weblogic
 FN_debug "Deploy RPD to Weblogic..."
 CURRENT_RPD=$(grep "^Star = " $INSTANCE_HOME/config/OracleBIServerComponent/coreapplication_obis1/NQSConfig.INI|awk '{print $3}'|sed 's/,//')
 [ $? -ne 0 ] && { FN_print "ERROR: Unable to access  $INSTANCE_HOME/config/OracleBIServerComponent/coreapplication_obis1/NQSConfig.INI to determine current RPD version!"; exit 1; }
 LS_CURRENT_RPD=$(ls -l $INSTANCE_HOME/bifoundation/OracleBIServerComponent/coreapplication_obis1/repository/$CURRENT_RPD)

  # Create PY script
  FN_print "INFO: Preparing the RPD Deployment Python script..."
  cd $RPD_BASE
#  echo "user = '"$WLSADMIN"'" >RPD_Deployment.py
#  echo "password = '"$WLSPWD"'" >>RPD_Deployment.py
  echo "host = '"$ADMIN_HOST":"$ADMIN_PORT"'" >RPD_Deployment.py
  echo "rpd_path = '"$RPD_BASE/$DFLT_RPD"'" >>RPD_Deployment.py
  echo "rpd_password = '"$CURR_PASS"'" >>RPD_Deployment.py
  echo "connect(userConfigFile='"$DOMAIN_HOME"/aserver/bifoundation_domain/configfileWL.secure',userKeyFile='"$DOMAIN_HOME"/aserver/bifoundation_domain/keyfile.secure',url=host)" >>RPD_Deployment.py
  echo "domainCustom()" >>RPD_Deployment.py
  echo "cd('oracle.biee.admin')" >>RPD_Deployment.py
  echo "# Locking the system configuration..." >>RPD_Deployment.py
  echo "print 'Obtaining a lock...'" >>RPD_Deployment.py
  echo "cd('oracle.biee.admin:type=BIDomain,group=Service')" >>RPD_Deployment.py
  echo "objs = jarray.array([], java.lang.Object)" >>RPD_Deployment.py
  echo "strs = jarray.array([], java.lang.String)" >>RPD_Deployment.py
  echo "try:" >>RPD_Deployment.py
  echo "    invoke( 'lock', objs, strs)" >>RPD_Deployment.py
  echo "except:" >>RPD_Deployment.py
  echo "    print 'Already locked'" >>RPD_Deployment.py
  echo "# Lock completed..." >>RPD_Deployment.py
  echo "cd('..')" >>RPD_Deployment.py
  echo "# Uploading the repository..." >>RPD_Deployment.py
  echo "print 'Uploading the $DFLT_RPD from $RPD_BASE/...'" >>RPD_Deployment.py
  echo "cd('oracle.biee.admin:type=BIDomain.BIInstance.ServerConfiguration,biInstance=coreapplication,group=Service')" >>RPD_Deployment.py
  echo "params = jarray.array([rpd_path,rpd_password],java.lang.Object)" >>RPD_Deployment.py
  echo "sign = jarray.array(['java.lang.String', 'java.lang.String'],java.lang.String)" >>RPD_Deployment.py
  echo "invoke( 'uploadRepository', params, sign)" >>RPD_Deployment.py
  echo "# Upload Complete" >>RPD_Deployment.py
  echo "cd('..')" >>RPD_Deployment.py
  echo "# Commit" >>RPD_Deployment.py
  echo "print 'Applying changes...'" >>RPD_Deployment.py
  echo "cd('oracle.biee.admin:type=BIDomain,group=Service')" >>RPD_Deployment.py
  echo "objs = jarray.array([], java.lang.Object)" >>RPD_Deployment.py
  echo "strs = jarray.array([], java.lang.String)" >>RPD_Deployment.py
  echo "try:" >>RPD_Deployment.py
  echo "    invoke('commit', objs, strs)" >>RPD_Deployment.py
  echo "except:" >>RPD_Deployment.py
  echo "    print 'Not Locked'" >>RPD_Deployment.py
  echo "# Commit Complete" >>RPD_Deployment.py
  echo "exit()" >>RPD_Deployment.py
  FN_debug "INFO: $(ls -l RPD_Deployment.py)" 


 FN_print "\nINFO: Backing up current RPD..." 
 mkdir -p $INSTANCE_HOME/bifoundation/OracleBIServerComponent/coreapplication_obis1/repository/archive
 cp $INSTANCE_HOME/bifoundation/OracleBIServerComponent/coreapplication_obis1/repository/$DFLT_RPD \
  $INSTANCE_HOME/bifoundation/OracleBIServerComponent/coreapplication_obis1/repository/archive/$DFLT_RPD.$(date +'%Y%d%m.%H%m%S') | tee -a $LOG_FILE
 [ $? -ne 0 ] && { echo "ERROR: problem backing up RPD"; exit 1; } 
 FN_print "INFO: RPD backup is $(ls -ltr $INSTANCE_HOME/bifoundation/OracleBIServerComponent/coreapplication_obis1/repository/archive/$DFLT_RPD.$(date +'%Y%d%m')*|tail -1)"
     
 FN_print "\n\nINFO: Deploying the new repository..."
  FN_debug "Deploying using WLS method..."  
  ADMIN_HOST=$(grep -A10 "<machine>ADMINHOST</machine>" $DOMAIN_HOME/aserver/bifoundation_domain/config/config.xml| grep listen-address | sed 's/<[^>]*>//g' | awk ' { print $1 } ')
  ADMIN_PORT=$(grep adminPort $INSTANCE_HOME/config/OPMN/opmn/instance.properties|sed 's/adminPort=//')
  FN_print "INFO: The derived hostname and WLS Admin ports are: $ADMIN_HOST:$ADMIN_PORT\n\n\n"; sleep 1
  $MW_HOME/oracle_common/common/bin/wlst.sh RPD_Deployment.py | tee -a $LOG_FILE
  FN_debug "Clearing up password and variables file..."
  cd $HOME/conf
  LATEST_RPD=$(grep "^Star = " $INSTANCE_HOME/config/OracleBIServerComponent/coreapplication_obis1/NQSConfig.INI|awk '{print $3}'|sed 's/,//')
  # For Non-DEV environments we remove the variables and password files.
  if [ $DEV_FLAG != Y ]
  then
  FN_print "INFO: Removing files related to $TWO_TASK for security."
  rm -rf $HOME/conf/password_file_${TWO_TASK} $HOME/conf/variables_file_${TWO_TASK}.xml
  fi
  
  FN_print "INFO: Your new RPD is $(ls -l $INSTANCE_HOME/bifoundation/OracleBIServerComponent/coreapplication_obis1/repository/$LATEST_RPD)"
  echo
 
 LT_LOG=$(ls -t $SCRIPT_DIR/*.log | head -n1)
 UP_CHK=$(grep 'Uploading the OracleBIAnalyticsApps' $LT_LOG | wc -l)
 if [ $UP_CHK = 1 ]
 then 
 FN_print "
#****************************************************************************************************
# Repository deployed successfully on the ${TWO_TASK} environment.
# Please check you can see ALL subject areas from the front end.
#****************************************************************************************************
  "
 else
 FN_print "
#****************************************************************************************************
# Repository deployment unsuccessful on the ${TWO_TASK} environment.
# Please check the Log File for more details.
#****************************************************************************************************
  "
 fi

 return 0
}