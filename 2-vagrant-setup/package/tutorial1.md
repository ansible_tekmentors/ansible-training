- name: Install latest version of "openjdk-6-jdk" ignoring "install-recommends"
  apt:
    name: openjdk-8-jdk
    state: latest
    install_recommends: no