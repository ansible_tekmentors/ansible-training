Ansible ships with several dynamic inventory scripts that one can use.
You can grab these by going to ansible GitHub repo and browsing to the contrib/inventory directory

Repo is https://github.com/ansible/ansible