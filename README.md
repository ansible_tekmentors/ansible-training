# Example to demonstrate uploading a new repository using WLST
#
# This scripts expects the following arguments:
#
# 1. wls.host (localhost)
# 2. wls.port (7001)
# 3. wls.user  (user1)
# 4. wls.password  (password1)
# 5. repository filename and location
# 6. repository password
# ===================================================================
 
import sys
import os
 
# Check that the arguments to this script are as expected.
# argv[0] is script name, [1]-[6] are the user parameters

argLen = len(sys.argv)
if argLen -1 != 6:
    print "ERROR: got ", argLen -1, " args."
    print "USAGE: wlst.cmd RPDUpload.py WLS_HOST WLS_PORT WLS_USER WLS_PASSWORD repository_location repository_password"
    print "   eg: wlst.cmd RPDUpload.py localhost 7001 weblogic welcome1 c:\SampleAppLite.rpd Admin123" 
    exit()
 
WLS_HOST = sys.argv[1]
WLS_PORT = sys.argv[2]
WLS_USER = sys.argv[3]
WLS_PW = sys.argv[4]
rpdlocation = sys.argv[5]
rpdpassword = sys.argv[6]

# Connect to the WLS Admin Server, so that the script then runs “online”
 
print 'Connecting to '+ WLS_HOST+ ':' + WLS_PORT + ' as user: ' + WLS_USER + ' ...'
 
connect(WLS_USER, WLS_PW, WLS_HOST+ ':' + WLS_PORT);

# Connect to the BIDomainMBean that controls the BI domain
# We will use this MBean to lock and then commit our config changes

print 'Connecting to Domain ...'
domainCustom()

cd ('oracle.biee.admin')

cd ('oracle.biee.admin:type=BIDomain,group=Service')

# define the MBean parameters and data types as arrays  
 
objs = jarray.array([],java.lang.Object)
strs = jarray.array([],java.lang.String)

# Invoke the lock operation from the BIDomainMBean
# (equivalent to the Lock and Edit Configuration button in 
# Enterprise Manager

print 'Locking the domain ...'

invoke('lock',objs,strs)

# Read the name of the first instance from first entry 
# in the BIInstances property within the BIDomainMBean
# (initially returned as an array, first value selected)

biinstances = get('BIInstances')
biinstance = biinstances[0]

# Connect to the corresponding BIInstanceMBean

print ('Connecting to BIInstance MBean')

cd ('..')
cd (biinstance.toString())

# Retrieve the name of the MBean for managing the 
# BI Server configuration

biserver = get('ServerConfiguration')

# Connect to the ServerConfigurationMBean for this BI Server

cd ('..')
cd (biserver.toString())

# Now prepare for the RPD upload
# Prepare arrays for parameters and datatypes
# Load the parameters with the RPD name and password
# Then invoke the uploadRepository within the ServerConfigurationMBean

print ('Uploading repository ...')

argtypes = jarray.array(['java.lang.String','java.lang.String'],java.lang.String)
argvalues = jarray.array([rpdlocation,rpdpassword],java.lang.Object)

invoke('uploadRepository',argvalues,argtypes)

# Now go back to the BIDomainMBean and commit the change
# (equivalent to the Activate Changes button in Enterprise Manager)

print ('Committing the update ...')

cd('..')
cd('oracle.biee.admin:type=BIDomain,group=Service')
objs = jarray.array([],java.lang.Object)
strs = jarray.array([],java.lang.String)

invoke('commit',objs,strs)

print ('Now restarting the instance, to pick up the new RPD file')

print 'Connecting to BIInstance MBean ...'
cd ('..')
cd (biinstance.toString())

# The BIInstanceMBean controls the overall status of
# the Oracle instance, and if directed to stop or start
# will stop/start all components together

print 'Getting instance status ...'

# ServiceStatus property in BIInstanceMBean returns
# the current status of the instance

servicestatus=get('ServiceStatus')
print 'BIInstance MBean; ServiceStatus: ' + servicestatus

# Stop action, if invoked, will stop entire instance

print 'Calling stop ...'
objs = jarray.array([], java.lang.Object)
strs = jarray.array([], java.lang.String)
invoke('stop', objs, strs)
servicestatus=get('ServiceStatus')

print 'BIInstance MBean; ServiceStatus: ' + servicestatus

# Start action will start the instance, and pick up
# the configuration change (the new RPD file path)

print 'Calling start ...'
objs = jarray.array([], java.lang.Object)
strs = jarray.array([], java.lang.String)
invoke('start', objs, strs)
servicestatus=get('ServiceStatus')

print 'BIInstance MBean; ServiceStatus: ' + servicestatus

print 'RPD Upload now complete!'

exit()

